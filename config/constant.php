<?php

    return [

        'api_source' => env('API_SOURCE',null),

        'api_user' => env('API_USER',null),
        
        'api_password' => env('API_PASSWORD',null),

    ];