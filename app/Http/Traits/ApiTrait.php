<?php 

namespace App\Http\Traits;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Config;

trait ApiTrait {

    public function __construct(){
        $this->base_url = Config::get('constant.api_source');
    }

    public function getToken($email,$password){        
        
        $res = Http::withBody('{
            "email": "'.$email.'",
            "password": "'.$password.'"
        }','application/json')->post($this->base_url.'api/v2/token');     
       
        return $res;
    }        

    public function getAuthors($orderBy = null,$direction = null,$limit = null,$page = null){        
        $res = Http::withToken(session()->get('access_token'))->get($this->base_url.'api/v2/authors?orderBy='.$orderBy.'&direction='.$direction.'&limit='.$limit.'&page='.$page);     
       
        return $res->json();
    }    

    public function addAuthor($data){
       
        $res = Http::withToken(session()->get('access_token'))->withBody($data,'application/json')->post($this->base_url.'api/v2/authors'); 
       
        return $res->status();
    }

    public function deleteAuthor($id){
       
        $res = Http::withToken(session()->get('access_token'))->delete($this->base_url.'api/v2/authors/'.$id); 
       
        return $res->status();
    }

    public function getAuthor($id){
        $res = Http::withToken(session()->get('access_token'))->get($this->base_url.'api/v2/authors/'.$id); 

        return $res->json();
    }

    public function getBooks($orderBy = null,$direction = null,$limit = null,$page = null){        
        $res = Http::withToken(session()->get('access_token'))->get($this->base_url.'api/v2/books?orderBy='.$orderBy.'&direction='.$direction.'&limit='.$limit.'&page='.$page);     
       
        return $res->json();
    }    

    
    public function addBook($data){
       
        $res = Http::withToken(session()->get('access_token'))->withBody($data,'application/json')->post($this->base_url.'api/v2/books'); 
       
        return $res->status();
    }


    public function getBook($id){
        $res = Http::withToken(session()->get('access_token'))->get($this->base_url.'api/v2/books/'.$id); 
        dd($res->json());
        return $res->json();
    }

    public function deleteBook($id){
       
        $res = Http::withToken(session()->get('access_token'))->delete($this->base_url.'api/v2/books/'.$id); 
       
        return $res->status();
    }

}