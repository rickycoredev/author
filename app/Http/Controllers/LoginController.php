<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\ApiTrait;
use Session;
class LoginController extends Controller
{
    use ApiTrait;
    public function index(){
        if(session::has('access_token')){
            return redirect('authors');
        }
        return view('template.auth.login');
    }

    public function login(Request $request){

        $token = $this->getToken($request->email,$request->password);
     
        if($token->status() == 200){
            session(['access_token' => $token->json()['token_key']]);
            session(['user' => $token->json()]);
            return redirect('authors');
        }
       
        session()->flash('error_message', 'Invalid Credentials!');        

        return back();
    }

    public function logout(){
        Session::flush();

        return redirect('login');
    }

}
