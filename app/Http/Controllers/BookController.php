<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\ApiTrait;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class BookController extends Controller
{
    use ApiTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $author_list = [];
        $books = $this->getBooks("id","ASC",$limit = 100,$page = 1);
        $authors = $this->getAuthors("id","ASC",$limit = 100,$page = 1);
        
        foreach($authors['items'] as $author){
            
            $author_list[$author['id']] = $author['first_name'].' '.$author['last_name'];
        }

        return view('pages.book.index')->with('books',$books)->with('authors',$author_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $release_date = Carbon::createFromFormat('Y-m-d H:i:s', $request->release_date.' 00:00:00');

        $book = array(
            "author" => ["id" => $request->author],
            "title" => $request->title,
            "release_date" => $release_date->toAtomString(),
            "description" => $request->description,
            "isbn" => $request->isbn,
            "format" => $request->format,
            "number_of_pages" => (int) $request->number_of_pages
        );

        $res = $this->addBook(json_encode($book));

        if($res == 200){
            session()->flash('message', 'Book created!');
        }else{
            session()->flash('error_message', 'Failed to add book!');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
        $book = $this->getBook($id);

        return view('pages.book.show')->with('book',$book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = $this->deleteBook((integer) $id);

        if($res == 204){
        session()->flash('message', 'Successfully deleted!');
        }else{
        session()->flash('error_message', 'Failed to delete book!');        
        }
        return redirect()->back();
    }
}
