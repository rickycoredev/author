<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\ApiTrait;
use Carbon\Carbon;

class AuthorController extends Controller
{
    use ApiTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()    {
        
        $authors = $this->getAuthors("id","ASC",$limit = 25,$page = 1);
        
        $author_list = [];
        foreach($authors['items'] as $author){
            $count = count($this->getAuthor($author['id'])['books']);
            $author['book_count'] = $count;
            array_push($author_list,$author);
        }
        return view('pages.author.index')->with('authors',$author_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $birthday = Carbon::createFromFormat('Y-m-d H:i:s', $request->birthday.' 00:00:00');
        

        $author = array("first_name" => $request->first_name,
                        "last_name" => $request->last_name,
                        "gender" => $request->gender,
                        "birthday" => $birthday->toAtomString(),
                        "biography" => $request->biography,
                        "place_of_birth" => $request->place_of_birth
        );

        $res = $this->addAuthor(json_encode($author));

        if($res == 200){
            session()->flash('message', 'Author created!');
        }else{
            session()->flash('error_message', 'Failed to add author!');
        }
        return redirect('/authors');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $author = $this->getAuthor((integer) $id);
       
        return view('pages.author.show')->with('author',$author);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
        $res = $this->deleteAuthor((integer) $id);

        if($res == 204){
        session()->flash('message', 'Successfully deleted!');
        }else{
        session()->flash('error_message', 'Fail to delete author!');        
        }
        return redirect('/authors');
    }
}
