<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
    return redirect('/login');
});

Route::get('/login', 'LoginController@index');
Route::post('/login','LoginController@login');

Route::group(['middleware'=>'role'], function(){    

    Route::resource('/authors','AuthorController');
    
    Route::resource('/books','BookController');
    
    Route::get('/logout','LoginController@logout');
});




