@extends('template.master')

@section('content')
    <div class="container-fluid">
        <b>Author</b> : {{ $author['first_name'].' '.$author['last_name'] }} <br>
        <b>Birthday</b> : {{ date('M-d-Y', strtotime($author['birthday'])) }}<br>
        <b>Gender</b> : {{ $author['gender'] }}<br>
        <b>Place of Birth</b> : {{ $author['place_of_birth'] }}<br>
        <b>Biography</b> <br>
        <p>
            {{ $author['biography'] }}
        </p>
    <br>
    <div class="row">
        <div class="col-lg-8">
             <h1>Books</h1>
        </div>
        <div class="col-lg-4">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addbook" style="float:right">add new book</button>

            <div class="modal" id="addbook">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form class="form-horizontal" method="POST" action="{{ route('books.store')}}">
                            {{ csrf_field() }}
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Add New Author</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                    <input type="hidden" name="author" value="{{ $author['id'] }}">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" placeholder="Enter Title" id="title" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="release_date">Release Date</label>
                                        <input type="date" name="release_date" class="form-control"  id="release_date" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control" cols="5" rows="5" required></textarea>
                                    </div>                                 
                                    <div class="form-group">
                                        <label for="isbn">ISBN</label>
                                        <input type="text" name="isbn" class="form-control" placeholder="Enter ISBN" id="isbn" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="format">Format</label>
                                        <input type="text" name="format" class="form-control" placeholder="Enter Format" id="format" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="number_of_pages">Number of Pages</label>
                                        <input type="text" name="number_of_pages" class="form-control" placeholder="Enter Number of Pages" id="number_of_pages" required>
                                    </div>

                                    

                                </div>

                                <!-- Modal footer -->
                                <div class="modal-footer">                                    
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> 
        </div>
    </div>
    <table id="author_book_list" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Title</th>
                <th>Release Date</th>
                <th>Description</th>
                <th>ISBN</th>
                <th>Format</th>
                <th># of Pages</th>
                <th>Action</th>
            </tr>
        </thead>
       <tbody>
            @foreach($author['books'] as $book)
            <tr>
                <td>{{ $book['title'] }}</td>
                <td>{{ date('M-d-Y', strtotime($book['release_date'])) }}</td>
                <td>{{ $book['description'] }}</td>
                <td>{{ $book['isbn'] }}</td>
                <td>{{ $book['format'] }}</td>
                <td>{{ $book['number_of_pages'] }}</td>
                <td><form action="{{ route('books.destroy', $book['id'])}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit"><i class="fas fa-trash"  ></i></button>
                </form></td>
            </tr>
            @endforeach
       </tbody>
        <tfoot>
            <tr>
                <th>Title</th>
                <th>Release Date</th>
                <th>Description</th>
                <th>ISBN</th>
                <th>Format</th>
                <th># of Pages</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
    </div>
@endsection

@section('custom_js')
<script>
    
$(document).ready(function () {
    var table = $('#author_book_list').DataTable();
});
</script>
@endsection