@extends('template.master')

@section('content')
<div class="author">
    <div class="row">
        <div class="col-lg-8">
             <h1>Author</h1>
        </div>
        <div class="col-lg-4">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addauthor"style="float:right" >add new author</button>
                
                    <!-- The Modal -->
                <div class="modal" id="addauthor">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form class="form-horizontal" method="POST" action="{{ route('authors.store')}}">
                            {{ csrf_field() }}
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Add New Author</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="text" name="first_name" class="form-control" placeholder="Enter First Name" id="first_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" name="last_name" class="form-control" placeholder="Enter Last Name" id="last_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name">Gender</label>
                                        <br>
                                        <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="male" name="gender" checked>Male
                                        </label>
                                        </div>
                                        <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="female" name="gender">Female
                                        </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="birthday">Birthday</label>
                                        <input type="date" name="birthday" class="form-control"  id="birthday" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="biography">Biography</label>
                                        <textarea name="biography" id="biography" class="form-control" cols="5" rows="5" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="place_of_birth">Place of Birth</label>
                                        <input type="text" name="place_of_birth" class="form-control" placeholder="Enter Place of Birth" id="place_of_birth" >
                                    </div>

                                </div>

                                <!-- Modal footer -->
                                <div class="modal-footer">                                    
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> 
        </div>
    </div>
    <table id="author_list" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Birthday</th>
                <th>Gender</th>
                <th>Place of Birth</th>
                <th>Action</th>
            </tr>
        </thead>
       <tbody>
            @foreach($authors as $author)
            <tr>
                <td><a href="/authors/{{ $author['id'] }}">{{ $author['first_name'].' '.$author['last_name'] }}</a></td>
                <td>{{ date('M-d-Y', strtotime($author['birthday'])) }}</td>
                <td>{{ $author['gender'] }}</td>
                <td>{{ $author['place_of_birth'] }}</td>
                <td >
                <form action="{{ route('authors.destroy', $author['id'])}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit" {{ $author['book_count'] != 0? 'disabled':'' }} ><i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Book Count : {{ $author['book_count'] }}"></i></button>
                </form></td>
            </tr>
            @endforeach
       </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Birthday</th>
                <th>Gender</th>
                <th>Place of Birth</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>

@endsection

@section('custom_js')
<script>
    
$(document).ready(function () {
    var table = $('#author_list').DataTable();
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
@endsection