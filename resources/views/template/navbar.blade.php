<div class="container" >
    <nav class="navbar navbar-expand-sm bg-primary" style="margin-bottom:50px;display:flex;flex-direction:row;justify-content: space-between">
    <ul class="navbar-nav" >
        <li class="nav-item">
        <a class="nav-link" href="/authors" style="color:white;">Authors</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/books" style="color:white;">Books</a>
        </li>
    </ul>
    <div class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown" style="color:white;">
            <i class="fas fa-user"></i> Welcome {{ session()->get('user')['user']['first_name'].' '.session()->get('user')['user']['last_name'] }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbardrop">
                <a class="dropdown-item" href="/logout" >Logout</a>
            </div>        
</div>
    </nav>
</div>
