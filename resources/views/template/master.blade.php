@include('template.header')

@include('template.alerts')

@yield('content')
   
@include('template.footer')
