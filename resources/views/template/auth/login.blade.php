@extends('template.master')

@section('title','Login | Assignment')

@section('custom_css')
<style>                  
 .login-container{
                height:100%;
                display: flex;
                align-items: center;
                justify-content: center;
                margin-top: 10%;
}

form{
	padding-top: 10px;
	font-size: 14px;
	margin-top: 30px;
}

.card-title{ font-weight:300; }

.btn{
	font-size: 14px;
	margin-top:20px;
}


.login-form{ 
	width:330px;
	margin:20px;
}

.sign-up{
	text-align:center;
	padding:20px 0 0;
}

.alert{
	margin-bottom:-30px;
	font-size: 13px;
	margin-top:20px;
}
</style>
@endsection

@section('content')
<div class="login-container">
	<div class="card login-form">
	<div class="card-body">
		<h3 class="card-title text-center">Log In</h3>
		<div class="card-text">
			<!--
			<div class="alert alert-danger alert-dismissible fade show" role="alert">Incorrect username or password.</div> -->
            <form class="form-horizontal" method="POST" action="/login">
				{{ csrf_field() }}
				<!-- to error: add class "has-danger" -->
				<div class="form-group">
					<label for="email">Email address</label>
					<input type="email" name="email" class="form-control form-control-sm" id="email" aria-describedby="emailHelp">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" class="form-control form-control-sm" id="password">
				</div>
				<button type="submit" class="btn btn-primary btn-block">Sign in</button>
			</form>
		</div>
	</div>
</div>
</div>
@endsection
