
<!-- ------------SETUP PROCESS-------------- -->

Step 1

Install PHP 
    Version 7.4 or higher 
    Option
    -- Manual php installation
    -- You can use local server like xampp,wamp or etc 

    after installation need to update some extension on php.ini (ex. given below)

    #OpenSSL PHP Extension
    #PDO PHP Extension
    #Mbstring PHP Extension
    #Tokenizer PHP Extension
    #XML PHP Extension

Step 2

Install Composer 
    Link : https://getcomposer.org/download/

    Option
    -- You can download exe file for composer please refer above link attached 
    -- You can install laravel composer programmatically using command given on the link

Step 3

Run this command on terminal to install laravel install globally
    -- composer global require "laravel/installer"

Step 4 

Run command below to create new laravel application
    -- laravel new ProjectName

If incase you have  existing repository project on gitlab,github or etc. that you want to clone

Step 1

Install git in your local (link provided below) 

    -- Link : https://git-scm.com/downloads

Step 2 

Clone existing project

Run command below
    -- git init
    -- git clone https://url_repository_link.git

Step 3

Go to project folder that you clone and run command below

    -- composer install
    -- php artisan key:generate

Step 4 Final

Serve your laravel application 
    
    -- php artisan serve



<!-- ------------------End of Setup Process------------------- -->


    



